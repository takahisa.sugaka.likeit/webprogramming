package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserDetailServlet
 */
@WebServlet("/UserDetailServlet")
public class UserDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserDetailServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//セッション開始
		HttpSession session = request.getSession();
		//セッションの中からuserInfoを取ってきてuserに代入
		User user = (User)session.getAttribute("userInfo");

		//userInfoがあるかどうかを確認
		if(user == null) {
			response.sendRedirect("LoginServlet");
			return;
		}

		// マルチバイト文字列の文字化け対応
		request.setCharacterEncoding("UTF-8");
		//urlパラメータから値を取得
		String id = request.getParameter("id");

		//取得した値をもとにユーザーを検索
		UserDao userDao = new UserDao();
		User userDetail = userDao.userFindById(id);
		//リクエスト領域に値をセット
		request.setAttribute("userDetail", userDetail);

		//ユーザー詳細のjspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_detail.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
