package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserListServlet
 */
@WebServlet("/UserListServlet")
public class UserListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserListServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//セッション開始
		HttpSession session = request.getSession();
		//セッションの中からuserInfoを取ってきてuserに代入
		User user = (User)session.getAttribute("userInfo");
		//userInfoがあるかどうかを確認
		if(user == null) {
			response.sendRedirect("LoginServlet");
			return;
		}

		//ユーザ一覧情報を取得
		UserDao userDao = new UserDao();
		List<User> userList = userDao.findAll();

		userList.remove(0);

		//リクエストスコープにユーザ一覧情報をセット
		request.setAttribute("userList", userList);

		//ユーザー一覧のjspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_list.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//リクエストパラメータの文字コード設定
		request.setCharacterEncoding("UTF-8");
		//UserDaoインスタンスの生成
		UserDao userDao = new UserDao();

		// リクエストパラメータの入力項目を取得
		String loginId = request.getParameter("loginId");
		String name = request.getParameter("name");
		//日付取得用変数
		String date1 = request.getParameter("date1");
		String date2 = request.getParameter("date2");


		//検索項目すべてが入力されているとき
		if(loginId !="" && name !="" && date1 != "" && date2 != "") {
			//検索結果を取得
			List<User> userList = userDao.userFindByParams(loginId, name, date1, date2);

			//リクエストスコープに検索結果をセット
			request.setAttribute("userList", userList);
		}

		//ログインIDだけが入力されているとき
		if(loginId != ""  && name.equals("") && date1.equals("") && date2.equals("")) {
			//検索結果を取得
			List<User> userList = userDao.userFindByParams(loginId);

			//リクエストスコープに検索結果をセット
			request.setAttribute("userList", userList);
		}

		//ログインIDと名前が入力されているとき
		if(loginId != ""  && name != "" && date1.equals("") && date2.equals("")) {
			//検索結果を取得
			List<User> userList = userDao.userFindByParams(loginId, name);

			//リクエストスコープに検索結果をセット
			request.setAttribute("userList", userList);
		}

		//ログインIDと誕生日が入力されているとき
		if(loginId != ""  && name.equals("") && date1 != "" && date2 != "") {
			//検索結果を取得
			List<User> userList = userDao.userFindByParams(loginId, date1, date2);

			//リクエストスコープに検索結果をセット
			request.setAttribute("userList", userList);
		}

		//ユーザー名のみが入力されているとき
		if(loginId.equals("") && name != "" && date1.equals("") && date2.equals("")) {
			//検索結果を取得
			List<User> userList = userDao.userFindByName(name);

			//リクエストスコープに検索結果をセット
			request.setAttribute("userList", userList);
		}

		//ユーザー名と生年月日が入力されているとき
		if(loginId.equals("") && name != "" && date1 != "" && date2 != "") {
			//検索結果を取得
			List<User> userList = userDao.userFindByName(name, date1, date2);

			//リクエストスコープに検索結果をセット
			request.setAttribute("userList", userList);
		}

		//生年月日が入力されているとき
		if(loginId.equals("") && name.equals("") && date1 != "" && date2 != "") {
			//検索結果を取得
			List<User> userList = userDao.userFindByBirthDate(date1, date2);

			//リクエストスコープに検索結果をセット
			request.setAttribute("userList", userList);

		}

		//ユーザー一覧のjspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_list.jsp");
		dispatcher.forward(request, response);
	}
}
