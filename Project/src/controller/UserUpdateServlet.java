package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserUpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserUpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//セッション開始
		HttpSession session = request.getSession();
		//セッションの中からuserInfoを取ってきてuserに代入
		User user = (User)session.getAttribute("userInfo");

		//userInfoがあるかどうかを確認
		if(user == null) {
			response.sendRedirect("LoginServlet");
			return;
		}

		// マルチバイト文字列の文字化け対応
		request.setCharacterEncoding("UTF-8");
		//urlパラメータから値を取得
		String id = request.getParameter("id");
		//userDaoの宣言
		UserDao userDao = new UserDao();

		//URLのIDを書き換えられないようにするための処理
		//idが取得できているかをチェック
		if (id != null) {
			int idInt = Integer.parseInt(id);
			if (idInt != user.getId() && user.getId() != 1) {
				User userDetail = userDao.userFindById(String.valueOf(user.getId()));
				//リクエスト領域に値をセット
				request.setAttribute("userDetail", userDetail);

				//更新jspにフォワード
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_update.jsp");
				dispatcher.forward(request, response);
			}
		}

		//取得した値をもとにユーザーを検索
		User userDetail = userDao.userFindById(id);
		//リクエスト領域に値をセット
		request.setAttribute("userDetail", userDetail);

		//更新jspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_update.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//リクエストパラメータの文字コード設定
		request.setCharacterEncoding("UTF-8");

		//リクエストパラメータの入力項目を取得
		String id = request.getParameter("id");
		String password = request.getParameter("password");
		String passwordConf = request.getParameter("passwordConf");
		String name = request.getParameter("name");
		String birthDate = request.getParameter("birthDate");

		if(!password.equals(passwordConf)) {
			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "入力された内容は正しくありません");

			UserDao userDao = new UserDao();
			User userDetail = userDao.userFindById(id);
			//リクエスト領域に値をセット
			request.setAttribute("userDetail", userDetail);

			// 更新jspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_update.jsp");
			dispatcher.forward(request, response);
			return;
		}

		if(password.equals(passwordConf)) {
			if(password.equals("")) {
				UserDao userDao = new UserDao();
				userDao.userUpdate(id, name, birthDate);
			}
			else {
				UserDao userDao = new UserDao();
				userDao.userUpdate(id, name, password, birthDate);
			}
		}

		//ユーザー一覧のサーブレットにリダイレクト
		response.sendRedirect("UserListServlet");


	}

}
