package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import model.User;

public class UserDao {
	public User findByLoginInfo(String loginId, String password) {
		Connection conn = null;
		try {
			//データベースへ接続
			conn = DBManager.getConnection();

			//暗号化メソッドを実行
			String hPassword = encryptionPassword(password);

			//SELECT文を準備
			String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

			//SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, hPassword);
			ResultSet rs = pStmt.executeQuery();

			if(!rs.next()) {
				return null;
			}

			//必要なデータのみインスタンスのフィールドに追加
			//ユーザ一覧画面で利用するためにidも取得するよう設定
			int id = rs.getInt("id");
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			return new User(id, loginIdData, nameData);

		}catch(SQLException e){
			e.printStackTrace();
			return null;
		}finally {
			//データベース切断
			if(conn != null) {
				try {
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public List<User> findAll() {
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			//データベースへ接続
			conn = DBManager.getConnection();

			//SELECT文を準備
			String sql = "SELECT * FROM user";

			//SELECT文を実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			//結果表に格納されたレコードの内容を
			//Userインスタンスに設定し、ArrayListインスタンスに追加
			while(rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);
				userList.add(user);
			}
		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}finally {
			//データベース切断
			if(conn != null) {
				try {
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	//検索項目全てが入力してある時のメソッド
	public List<User> userFindByParams(String searchLoginId, String searchName, String date1, String date2) {
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			//データベースへ接続
			conn = DBManager.getConnection();

			//SELECT文を準備
			String sql = "SELECT * FROM user WHERE login_id = ? and name LIKE ?  and birth_date >= ? and birth_date <= ? and not id = 1";

			//SELECT文を実行
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, searchLoginId);
			pStmt.setString(2, "%" + searchName + "%");
			pStmt.setString(3, date1);
			pStmt.setString(4, date2);

			ResultSet rs = pStmt.executeQuery();
			//結果表に格納されたレコードの内容を
			//Userインスタンスに設定し、ArrayListインスタンスに追加
			while(rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);
				userList.add(user);
			}
		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}finally {
			//データベース切断
			if(conn != null) {
				try {
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}

		return userList;
	}

	//ログインIDだけで検索するとき
	public List<User> userFindByParams(String searchLoginId) {
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			//データベースへ接続
			conn = DBManager.getConnection();

			//SELECT文を準備
			String sql = "SELECT * FROM user WHERE login_id = ? and not id = 1";

			//SELECT文を実行
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, searchLoginId);


			ResultSet rs = pStmt.executeQuery();
			//結果表に格納されたレコードの内容を
			//Userインスタンスに設定し、ArrayListインスタンスに追加
			while(rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);
				userList.add(user);
			}
		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}finally {
			//データベース切断
			if(conn != null) {
				try {
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}

		return userList;
	}

	//ログインIDと名前で検索するとき
	public List<User> userFindByParams(String searchLoginId, String searchName) {
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			//データベースへ接続
			conn = DBManager.getConnection();

			//SELECT文を準備
			String sql = "SELECT * FROM user WHERE login_id = ? and name LIKE ? and not id = 1";

			//SELECT文を実行
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, searchLoginId);
			pStmt.setString(2, "%" + searchName + "%");

			ResultSet rs = pStmt.executeQuery();
			//結果表に格納されたレコードの内容を
			//Userインスタンスに設定し、ArrayListインスタンスに追加
			while(rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);
				userList.add(user);
			}
		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}finally {
			//データベース切断
			if(conn != null) {
				try {
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}

		return userList;

	}

	//ログインIDと生年月日でユーザー検索を行うメソッド
	public List<User> userFindByParams(String searchLoginId, String date1, String date2) {
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			//データベースへ接続
			conn = DBManager.getConnection();

			//SELECT文を準備
			String sql = "SELECT * FROM user WHERE login_id = ? and birth_date >= ? and birth_date <= ? and not id = 1";

			//SELECT文を実行
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, searchLoginId);
			pStmt.setString(2, date1);
			pStmt.setString(3, date2);

			ResultSet rs = pStmt.executeQuery();
			//結果表に格納されたレコードの内容を
			//Userインスタンスに設定し、ArrayListインスタンスに追加
			while(rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);
				userList.add(user);
			}
		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}finally {
			//データベース切断
			if(conn != null) {
				try {
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}

		return userList;

	}

	//名前のみでユーザ検索を行うメソッド
	public List<User> userFindByName(String searchName){
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			//データベースへ接続
			conn = DBManager.getConnection();

			//SELECT文を準備
			String sql = "SELECT * FROM user WHERE name LIKE ? and not id = 1";

			//SELECT文を実行
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, "%" + searchName + "%");

			ResultSet rs = pStmt.executeQuery();
			//結果表に格納されたレコードの内容を
			//Userインスタンスに設定し、ArrayListインスタンスに追加
			while(rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);
				userList.add(user);
			}
		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}finally {
			//データベース切断
			if(conn != null) {
				try {
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	//名前と生年月日で検索するメソッド
	public List<User> userFindByName(String searchName, String date1, String date2) {
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			//データベースへ接続
			conn = DBManager.getConnection();

			//SELECT文を準備
			String sql = "SELECT * FROM user WHERE name LIKE ? and birth_date >= ? and birth_date <= ? and not id = 1";

			//SELECT文を実行
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, "%" + searchName + "%");
			pStmt.setString(2, date1);
			pStmt.setString(3, date2);

			ResultSet rs = pStmt.executeQuery();
			//結果表に格納されたレコードの内容を
			//Userインスタンスに設定し、ArrayListインスタンスに追加
			while(rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);
				userList.add(user);
			}
		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}finally {
			//データベース切断
			if(conn != null) {
				try {
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	//生年月日で検索するメソッド
	public List<User> userFindByBirthDate(String date1, String date2) {
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			//データベースへ接続
			conn = DBManager.getConnection();

			//SELECT文を準備
			String sql = "SELECT * FROM user WHERE birth_date >= ? and birth_date <= ? and not id = 1";

			//SELECT文を実行
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, date1);
			pStmt.setString(2, date2);

			ResultSet rs = pStmt.executeQuery();
			//結果表に格納されたレコードの内容を
			//Userインスタンスに設定し、ArrayListインスタンスに追加
			while(rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);
				userList.add(user);
			}
		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}finally {
			//データベース切断
			if(conn != null) {
				try {
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}


	public void userRegistration(String loginId, String name, String birthDate, String password) {
		Connection conn = null;
		try {
			//データベースへ接続
			conn = DBManager.getConnection();

			//暗号化メソッドを実行
			String hPassword = encryptionPassword(password);

			//INSERT文を準備
			String sql = "INSERT into user(login_id, name, birth_date, password, create_date, update_date) values(?, ?, ?, ?, now(), now())";

			//INSERT文を実行
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, name);
			pStmt.setString(3, birthDate);
			pStmt.setString(4, hPassword);


			pStmt.executeUpdate();

		}catch(SQLException e){
			e.printStackTrace();
		}finally {
			//データベース切断
			if(conn != null) {
				try {
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public User findByLoginId(String loginId) {
		Connection conn = null;
		try {
			//データベースへ接続
			conn = DBManager.getConnection();

			 // SELECT文を準備
            String sql = "SELECT * FROM user WHERE login_id = ?";

             // SELECTを実行し、結果表を取得
            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, loginId);
            ResultSet rs = pStmt.executeQuery();


            // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
            if (!rs.next()) {
                return null;
            }

            // 必要なデータのみインスタンスのフィールドに追加
            String loginIdData = rs.getString("login_id");
            return new User(loginIdData);

		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}finally {
			//データベース切断
			if(conn != null) {
				try {
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public User userFindById(String userId) {
		Connection conn = null;
		try {
			//データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
            String sql = "SELECT * FROM user WHERE id = ?";

            // SELECTを実行し、結果表を取得
            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, userId);
            ResultSet rs = pStmt.executeQuery();

            // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
            if (!rs.next()) {
                return null;
            }

            // メソッドを使い回すためにインスタンスにはフィールド情報を全部追加する。
            //jspで必要なフィールド情報だけを出力するようにする
            int id = rs.getInt("id");
            String loginIdData = rs.getString("login_id");
            String nameData = rs.getString("name");
            Date birthDate = rs.getDate("birth_date");
            String password = rs.getString("password");
            String createDate = rs.getString("create_date");
            String updateDate = rs.getString("update_date");
            //↑を元にユーザーインスタンスを作成
            return new User(id, loginIdData, nameData, birthDate, password, createDate, updateDate);
		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}finally {
			if(conn != null) {
				try {
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	//名前と生年月日を更新するメソッド
	public void userUpdate(String id, String name, String birthDate) {
		Connection conn = null;
		try {
			//データベースへ接続
			conn = DBManager.getConnection();

			//UPDATE文を準備
			String sql = "UPDATE user SET name= ?,birth_date= ?, update_date = now() WHERE id = ?";

			// UPDATE文を実行
            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, name);
            pStmt.setString(2, birthDate);
            pStmt.setString(3, id);

            pStmt.executeUpdate();

		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			if(conn != null) {
				try {
					//データベース切断
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	//名前、生年月日、パスワードを更新するメソッド
	public void userUpdate(String id, String name, String password, String birthDate) {
		Connection conn = null;

		try {
			//データベースへ接続
			conn = DBManager.getConnection();

			//暗号化メソッドを実行
			String hPassword = encryptionPassword(password);

			//UPDATE文を準備
			String sql ="UPDATE user SET name= ?, birth_date= ?, password = ?, update_date = now() WHERE id = ?";

			//UPDATE文を実行
			 PreparedStatement pStmt = conn.prepareStatement(sql);
	         pStmt.setString(1, name);
	         pStmt.setString(2, birthDate);
	         pStmt.setString(3, hPassword);
	         pStmt.setString(4, id);

	         pStmt.executeUpdate();
		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			if(conn !=null) {
				try {
					//データベース切断
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void userDelete(String id) {
		Connection conn = null;

		try {
			//データベースへ接続
			conn = DBManager.getConnection();

			//DELETE文を準備
			String sql = "DELETE FROM user WHERE id = ?";

			//DELETE文を実行
			 PreparedStatement pStmt = conn.prepareStatement(sql);
	         pStmt.setString(1, id);

	         pStmt.executeUpdate();
		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			if(conn !=null) {
				try {
					//データベース切断
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}



	//パスワード暗号化メソッド
	String encryptionPassword(String password){
		try {
			//ハッシュを生成したい元の文字列
			String source = password;
			//ハッシュ生成前にバイト配列に置き換える際のCharset
			Charset charset = StandardCharsets.UTF_8;
			//ハッシュアルゴリズム
			String algorithm = "MD5";

			//ハッシュ生成処理
			byte[] bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
			String hPassword = DatatypeConverter.printHexBinary(bytes);
			return hPassword;
		}catch(NoSuchAlgorithmException e){
			e.printStackTrace();
			return null;
		}

	}

}
