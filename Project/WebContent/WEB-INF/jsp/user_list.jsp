<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>ユーザ一覧</title>
    <link rel="stylesheet"
          href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
          crossorigin="anonymous">
    <link href="css/style.css" rel="stylesheet" type="text/css" />

</head>

	<body>
        <header>
            <nav class="navbar navbar-expand-sm  bg-dark">
                <div class="container justify-content-end">
                    <ul class="navbar-nav">
                        <li class="user-name nav-item">
                            ${userInfo.name} さん
                        </li>
                        <li class="nav-item">
                           <a class="logout nav-link" href="LogoutServlet">ログアウト</a>
                        </li>

                    </ul>
                </div>
            </nav>
        </header>

    <main>
    	<div class="container">
    		<div class="col-sm-6 mx-auto text-center title-area">
                <h1>ユーザ一覧</h1>
            </div>
            <div class="row">
            	<div class="col-sm-6 mx-auto">

                    <div class="row">
                        <div class="resistration_link">
                        	<c:if test= "${userInfo.id == 1}">
                                <a href="UserRegisterServlet">新規登録</a>
                        	</c:if>
                        </div>
                    </div>
                    <form action="UserListServlet" method="post">
                        <div class="form-group">
                            <label class="user-id">ログインID</label>
                            <input type="text" name="loginId" style="width:250px;" class="form-control">
                        </div>
                        <div class="form-group">
                            <label class="user-form">ユーザ名</label>
                            <input type="text" name="name" style="width:250px;" class="form-control">
                        </div>
                        <div class="form-group">
                            <label class="user-form">生年月日</label>
                            <input type="date" name="date1" style="width:200px;" class="form-control">
                             〜　
                            <input type="date" name="date2" style="width:150px;" class="form-control">
                        </div>
                        <div class="search-button">
                            <input type="submit" value="検索" class="btn btn-gray ">
                        </div>
                    </form>
            	</div>
            </div>
    	</div>
		<div class="table">
        	<hr style="border:0;border-top:1px solid ;">
            <table border="0" class="col-sm-8 mx-auto">
            	<tr>
                	<th>ログインID</th>
                    <th>ユーザー名</th>
                    <th>生年月日</th>
                    <th>  </th>
                </tr>
                    <c:forEach var="user" items="${userList}">
	                    <tr>
		                    <td>${user.loginId}</td>
		                    <td>${user.name}</td>
		                    <td>${user.birthDate}</td>
		                    <td>
		                    	<a class="btn btn-blue" href="UserDetailServlet?id=${user.id}">詳細</a>
		                        	<c:choose>
		                            	<c:when test= "${userInfo.id == 1}">
		                            		<a class="btn btn-green" href="UserUpdateServlet?id=${user.id}">更新</a>
		                            	</c:when>
		                            	<c:when test= "${userInfo.id == user.id}">
		                            		<a class="btn btn-green" href="UserUpdateServlet?id=${user.id}">更新</a>
		                            	</c:when>
		                            </c:choose>
									<c:if test= "${userInfo.loginId == 'admin'}">
										<a class="btn btn-red" href ="UserDeleteServlet?id=${user.id}">削除</a>
									</c:if>
		                    </td>
	                    </tr>
                    </c:forEach>
                </table>
            </div>
		</main>
	</body>
</html>