<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

	<head>
	    <meta charset="UTF-8">
	    <title>ユーザ情報詳細</title>
	    <link rel="stylesheet"
	          href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	          crossorigin="anonymous">
	    <link href="css/style.css" rel="stylesheet" type="text/css" />

	</head>

	<body>
		<header>
	    	<nav class="navbar navbar-expand-sm  bg-dark">
	        	<div class="container justify-content-end">
	            	<ul class="navbar-nav">
	                	<li class="user-name nav-item">
	                    	${userInfo.name} さん
	                    </li>
	                    <li class="nav-item">
	                    	<a class="logout nav-link" href="LogoutServlet">ログアウト</a>
	                    </li>
					</ul>
				</div>
			</nav>
		</header>

        <main>
            <div class="container">
                <div class="col-sm-6 mx-auto text-center title-area">
                    <h1>ユーザ情報詳細参照</h1>
                </div>

                <div class="row">
                    <div class="col-sm-6 mx-auto">
                        <ul class="user-attribute">
                        <li class="list">
                            ログインID
                        </li>
                        <li class="list">
                            ユーザ名

                        </li>
                        <li class="list">
                            生年月日
                        </li>
                        <li class="list">
                            登録日時
                        </li>
                        <li class="list">
                            更新日時
                        </li>
                        </ul>

                        <ul class="user-detail">
                        <li class="list">
                            ${userDetail.loginId}
                        </li>
                        <li class="list">
                            ${userDetail.name}
                        </li>
                        <li class="list">
							${userDetail.birthDate}
                        </li>
                        <li class="list">
                            ${userDetail.createDate}
                        </li>
                        <li class="list">
                            ${userDetail.updateDate}
                        </li>
                        </ul>

                        <div class="row">
                            <div class="pageback_link">
                                <a href="UserListServlet">戻る</a>
                            </div>
                        </div>
                    </div>


                </div>

            </div>
        </main>
    </body>
</html>