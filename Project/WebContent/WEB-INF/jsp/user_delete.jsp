<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

	<head>
	    <meta charset="UTF-8">
	    <title>ユーザ削除確認</title>
	    <link rel="stylesheet"
	          href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	          crossorigin="anonymous">
	    <link href="css/style.css" rel="stylesheet" type="text/css" />

	</head>

	<body>
		<header>
	    	<nav class="navbar navbar-expand-sm  bg-dark">
	        	<div class="container justify-content-end">
	            	<ul class="navbar-nav">
	                	<li class="user-name nav-item">
	                    	${userInfo.name} さん
	                    </li>
	                     <li class="nav-item">
	                     	<a class="logout nav-link" href="LogoutServlet">ログアウト</a>
	                     </li>

	                </ul>
	 			</div>
			</nav>
	 	</header>

	    <main>
	    	<div class="container">
	        	<div class="col-sm-6 mx-auto text-center title-area">
	                <h1>ユーザ削除確認</h1>
	            </div>



	            <div class="row">
	                <div class="col-sm-6 mx-auto">
	                    <div class="del-message">
	                        <p>
	                            ログインid：${userDetail.loginId} <br>
	                            を本当に削除してもよろしいでしょうか。
	                        </p>
	                    </div>

	                    <div class="button-area">
	                    	<form action="UserDeleteServlet" method="post">
	                    		<input name="id" type="hidden" value="${userDetail.id}">
		                        <a  href="UserListServlet" style="width:120px" class="btn btn-gray cancel">キャンセル</a>
		                        <input type="submit" value="OK" style="width:120px" class="btn btn-gray ok">
	                        </form>
	                    </div>
	                </div>
	            </div>

	       </div>
	   </main>
	</body>
</html>