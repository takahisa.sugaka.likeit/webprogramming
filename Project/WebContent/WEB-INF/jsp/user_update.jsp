<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>ユーザ情報更新</title>
    <link rel="stylesheet"
          href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
          crossorigin="anonymous">
    <link href="css/style.css" rel="stylesheet" type="text/css" />

</head>

<body>
    <header>
        <header>
            <nav class="navbar navbar-expand-sm  bg-dark">
                <div class="container justify-content-end">
                    <ul class="navbar-nav">
                        <li class="user-name nav-item">
                            ${userInfo.name} さん
                        </li>
                        <li class="nav-item">
                           <a class="logout nav-link" href="LogoutServlet">ログアウト</a>
                        </li>

                    </ul>
                </div>
            </nav>
        </header>

         <main>
            <div class="container">
                <div class="col-sm-6 mx-auto text-center title-area">
                    <h1>ユーザ情報更新</h1>
                </div>

                <c:if test="${errMsg != null}" >
				    <div class="alert alert-danger" role="alert">
					  ${errMsg}
					</div>
				</c:if>

                <div class="row">
                    <div class="col-sm-6 mx-auto">
                        <form action="UserUpdateServlet" method="post">
                        <input name="id" type="hidden" value="${userDetail.id}">
                            <div class="form-group">
                                <label class="user-id">ログインID</label>
                                ${userDetail.loginId}
                            </div>
                            <div class="form-group">
                                <label class="user-form">パスワード</label>
                                <input type="password" name="password" style="width:200px;" class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="user-form">パスワード(確認)</label>
                                <input type="password" name="passwordConf" style="width:200px;" class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="user-form">ユーザ名</label>
                                <input type="text" name="name" value="${userDetail.name}" style="width:200px;" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label class="user-form">生年月日</label>
                                <input type="text" name="birthDate" value="${userDetail.birthDate}" style="width:200px;" class="form-control" required>
                            </div>

                            <div class="button-area">
	                            <input type="submit" value="更新" class="btn btn-gray ">
	                        </div>
                        </form>

                        <div class="row">
                            <div class="pageback_link">
                                    <a href="UserListServlet">戻る</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </body>
</html>