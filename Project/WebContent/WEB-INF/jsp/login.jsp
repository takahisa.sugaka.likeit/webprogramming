<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>ログイン</title>
    <link rel="stylesheet"
          href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
          crossorigin="anonymous">
    <link href="css/style.css" rel="stylesheet" type="text/css" />

</head>

<body>
    <div class="container">
        <div class="col-sm-6 mx-auto text-center title-area">
            <h1>ログイン画面</h1>
        </div>

		<c:if test="${errMsg != null}" >
		    <div class="alert alert-danger" role="alert">
			  ${errMsg}
			</div>
		</c:if>

        <div class="row">
            <div class="col-sm-6 mx-auto">
                <form class="form-login" action="LoginServlet" method="post">
                    <div class="form-group">
                        <label class="login-form">ログインID</label>
                        <input type="text" name="loginId" id="inputLoginId" style="width:200px;" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label class="login-form">パスワード</label>
                        <input type="password" name="password" id="inputPassword" style="width:200px;" class="form-control" required>
                    </div>

                    <div class="button-area">
                        <input type="submit" value="ログイン" class="btn btn-gray ">
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>
</html>