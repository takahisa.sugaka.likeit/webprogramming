CREATE DATABASE usermanagement DEFAULT CHARSET utf8;

USE usermanagement;

CREATE TABLE user(
    id SERIAL UNIQUE,
    login_id varchar(255) UNIQUE NOT NULL,
    name varchar(255) NOT NULL,
    birth_date DATE NOT NULL,
    password varchar(255) NOT NULL,
    create_date DATETIME NOT NULL,
    update_date DATETIME NOT NULL
);

INSERT INTO user VALUES(
    1,
    'admin',
    '管理者',
    '1993-03-02',
    'password',
    NOW(),
    NOW()
);